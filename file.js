// Copyright (c) 2020 - 2021 Paul Raffer


function readFile(filePath)
{
	var request = new XMLHttpRequest();
	request.open("GET", filePath, false);
	request.send();
	return request.responseText;
}


function include(filePath)
{
	document.write(readFile(filePath));
}

